package com.xa;

import java.util.Scanner;

public class SoalNo7Main {
    public static void main(String[] args){
        System.out.println("Masukan kata");
        Scanner scan = new Scanner(System.in);
        String kata = scan.nextLine();

        char [] huruf = kata.toCharArray();

        int jmlBintang = huruf.length/2;
        char[] bntg = new char[jmlBintang];

        for (int i = 0; i < jmlBintang; i++ ){
            bntg[i] ='*';
        }

        String bintang = new String(bntg);

        for (int i = 0; i <huruf.length ; i++ ){
            System.out.println(bintang+huruf[i]+bintang);
        }

    }
}
