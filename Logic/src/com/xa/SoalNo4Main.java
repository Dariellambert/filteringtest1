package com.xa;


import java.util.Scanner;

public class SoalNo4Main {
    public static void main (String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.print("masukan deret: ");
        String[] input = scan.nextLine().split(" ");
        int [] num = convertInt(input);

        for (int i = 0; i < num.length; i++){ //bubble sort
            for (int j = 0; j < num.length; j++){
                if (num[i] < num[j]){
                    int temp = num[i];
                    num[i] = num[j];
                    num[j] = temp;
                }
            }
        }

        for (int i = 0; i < num.length; i++){
            System.out.println(num[i]);
        }

        int sumMax =0;
        int sumMin = 0;

        for (int i = 0; i < 4; i++){
            sumMin += num[i];
        }

        for (int i = num.length-1; i > (num.length-1)-4; i--){
            sumMax += num[i];
        }

        System.out.println("\nnilai 4 total max: "+ sumMax+" nilai 4 total min: "+sumMin);
    }

    public static int[] convertInt(String[] input){
        int[] temp = new int[input.length];
        for (int i =0; i < input.length; i++){
            temp[i] = Integer.parseInt(input[i]);
        }
        return temp;
    }
}
