package com.xa;

import java.util.ArrayList;
import java.util.Scanner;

public class SoalNo3Main {
    public static void main (String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Masukan input: ");
        String[] input = scan.nextLine().split(" ");
        int[] num = convertInt(input);

        int hasilMedian = cariMedian(num);
        double hasilMean = cariMean(num);
        int hasilModus = cariModus(num);

        System.out.println("hasil median dari deretan angka adalah: "+hasilMedian);
        System.out.println("hasil mean dari deretan angka adalah: "+hasilMean);
        System.out.println("hasil modus dari deretan angka adalah: "+hasilModus);


    }

    public static int[] convertInt(String[] input){
        int[] temp = new int[input.length];
        for (int i =0; i < input.length; i++){
             temp[i] = Integer.parseInt(input[i]);
        }
        return temp;
    }

    public static int cariMedian(int[] num){
        for (int i =0; i < num.length; i++){
            for (int j =0; j < num.length;j++){
                if(num[i] < num[j]){
                    int temp = num[i];
                    num[i] = num[j];
                    num[j]= temp;
                }
            }
        }
        int med =0;
        int posMed = num.length/2;
        if(num.length % 2 == 1){
            med = num[posMed];
            return med;
        }
        else if (num.length % 2 == 0){
            med = num[posMed+1];
            return med;
        }
        return 0;

    }

    public static double cariMean(int []num){
        //D
        double[] dum  =new double[num.length];
        for (int i = 0; i < num.length; i++){
            dum[i] = Double.valueOf(num[i]);
        }
        double temp = 0;
        for (double d: dum){
            temp +=d;
        }
        double mean = temp/num.length;
        return mean;
    }

    public static int cariModus(int[] array) {
        ArrayList <Integer> besar = new ArrayList<>();
        int modus = 0;
        int maxCount = 0;
        for (int i = 0; i < array.length; i++) {
            int value = array[i];
            int count = 0;
            for (int j = 0; j < array.length; j++) {
                if (array[j] == value) count++;
                if (count > maxCount) {
                    modus = value;
                    besar.add(modus);
                    maxCount = count;
                }
            }
        }
        int max =0;
        for (int i =0; i < besar.size(); i++){
            if(besar.get(i) > max){
                max = besar.get(i);
            }
        }
        return max;
       // if (maxCount > 1) {
         //   return max;
        //}
        //return 0;
    }
}
