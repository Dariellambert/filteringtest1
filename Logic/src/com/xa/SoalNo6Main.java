package com.xa;

import java.util.Scanner;

public class SoalNo6Main {
    public static void main (String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.print(" Masukan kalimat: ");
        String[] kalimat = scan.nextLine().split(" ");
        for (String kata : kalimat) {
            String newHurufAwal = "";
            String newHurufAkhir = "";
            for (int i = 0; i < kata.length(); i++) {
                if (i == 0) {
                    newHurufAwal += Character.toString(kata.charAt(i));
                }
                if (i == kata.length() - 1) {
                    newHurufAkhir += Character.toString(kata.charAt(i));
                }
            }
            System.out.print(newHurufAwal + "***"+ newHurufAkhir+ " ");
        }
    }
}
