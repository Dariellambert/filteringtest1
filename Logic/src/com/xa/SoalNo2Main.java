package com.xa;

import java.util.Scanner;

public class SoalNo2Main {
    public static void main (String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Masukan jumlah deret:") ;
        int n = scan.nextInt();
        int[] num = new int[n];

        System.out.println("Masukan angka: ") ;
        for(int i=0;i<n;i++)
        {
            num[i] = scan.nextInt();
        }

        System.out.println("Masukan jml pergeseran: ");
        int shift = scan.nextInt();

        System.out.println("deret angka: ");
        for(int i=0;i<n;i++)
        {
            System.out.print(num[i]+" ");
        }

        System.out.println();
        System.out.println("1. shift right");
        System.out.println("2. shift left");
        System.out.print("pilihan: ");
        int pilih = scan.nextInt();
        switch (pilih){
            case 1:
                shiftRight(n,num,shift);
                break;
            case 2:
                shiftLeft(n,num,shift);
                break;
            default:
                break;
        }
    }

    public static void shiftRight(int n, int[] num, int shift){
        int temp;

        for (int i =0; i < shift; i++){
            temp=num[0];
            for(int j= 0;j < n-1;j++)
            {
                num[j]=num[j+1];
            }
            num[n-1]=temp;
            //System.out.println("\nderet angka digeser");

        }
        for(int k=0;k<n;k++)
        {
            System.out.print(num[k]+" ");
        } // 4 5 6 7 1 2 3

    }

    public static void shiftLeft(int n, int[] num, int shift){
        int temp;
        for (int i =0; i < shift; i++){
            temp=num[n-1];
            for(int j=n-1;j>0;j--)
            {
                num[j]=num[j-1];
            }
            num[0]=temp;
            //System.out.println("\nderet angka digeser");
            for(int k=0;k<n;k++)
            {
                System.out.print(num[k]+" ");
            }
        }

    }
}
