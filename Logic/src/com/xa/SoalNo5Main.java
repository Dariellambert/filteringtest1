package com.xa;

import java.util.Scanner;

public class SoalNo5Main {
    public static void main (String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.print("masukan deret: ");
        String[] input = scan.nextLine().split(" ");
        int[] num = convertInt(input);
        int sorted[] = bubbleSortAsc(num);

        for (int s: sorted)
            System.out.print(s+" ");
    }

    public static int[] convertInt(String[] input){
        int[] temp = new int[input.length];
        for (int i =0; i < input.length; i++){
            temp[i] = Integer.parseInt(input[i]);
        }
        return temp;
    }

    public static int[] bubbleSortAsc(int[] num){
        for (int i =0; i < num.length; i++){
            for( int j =0; j < num.length; j++){
                if (num[i] < num[j]){
                    int temp = num[i];
                    num[i] = num[j];
                    num[j] = temp;
                }
            }
        }
        return num;
    }
}
