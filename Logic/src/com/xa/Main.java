package com.xa;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //System.out.println("Soal 01");
        String awal, balik = "";
        Scanner in = new Scanner(System.in);
        System.out.print("Input: ");
        awal = in.nextLine();

        //int length = awal.length();

        for (int i = awal.length() - 1; i >= 0; i--)
            balik += Character.toString(awal.charAt(i));

        System.out.println("Output: "+balik);
        if (awal.equals(balik))
            System.out.println("Yes");
        else
            System.out.println("No");

    }
}
